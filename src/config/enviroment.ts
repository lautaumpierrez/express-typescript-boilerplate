import * as dotenv from 'dotenv';


// exporting config method to globalize process.<env> variables from <root>/.env 
let {config} = dotenv;
export default config;
