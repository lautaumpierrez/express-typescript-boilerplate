import * as express from 'express';
import RootRouter from './routes/';
import Autoload from './config/autoload';

class App {
  public app: express.Application;
  private port:number = 4000;
  private rootRouter:any;
  constructor(){
    this.app = express();
    this.config();
  }
  public start(cb:Function){
    this.app.listen(this.port, ()=>{
      console.log('listening at http://localhost:4000');

    })
  }
  public config():void{
    this.app.use(express.json());
    this.rootRouter = new RootRouter(this.app);
    Autoload();
  }
}

export default new App();
