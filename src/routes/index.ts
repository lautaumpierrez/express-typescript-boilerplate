
import UsersRoutes from './Users';

class RootRouter{
  constructor(app:any){
    app.use('/users',UsersRoutes);
  }
}

export default RootRouter;
