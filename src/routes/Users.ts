
import {Router} from 'express';
import UserController from '../controllers/UserController';
class UsersRoutes {
  router:any;
  // <method>_routes
  constructor(){
    this.router = Router();
    this.get_Routes();
  }
  public get_Routes(){
    this.router.get('/', UserController.getUsers);
  }
}

export default new UsersRoutes().router;
